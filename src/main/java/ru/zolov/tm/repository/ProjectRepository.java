package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {
    private List<Project> projectList = new ArrayList<Project>();

    public ProjectRepository() {
    }

    public void create(String name) {
        projectList.add(new Project(name));
    }

    public void readAll() {
        for (Project p : projectList) {
            System.out.println(p);
        }
    }

    public void update(int id, String name) {
        for (Project p : projectList) {
            if (id == p.getProjectId()) {
                p.setProjectName(name);
            }
        }
        System.out.println("New project name " + name);
    }

    public void delete(int id) {
        for (Project p : projectList) {
            if (id == p.getProjectId()) {
                projectList.remove(p);
            }
        }
    }

}
