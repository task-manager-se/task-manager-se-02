package ru.zolov.tm.repository.api;

public interface EntityRepository {

    void create(String name);

    void readAll();

    void readById(int id);

    void update(int id, String name);

    void delete(int id);

    void deleteAll();


    void createTask(int id);

    void readTaskByProjId(int id);

    void readTaskById(int id);

    void updateTask(int id, String description);

    void deleteTask(int id);

    void deleteAllTasks();

}
