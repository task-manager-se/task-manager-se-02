package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {
    private List<Task> taskList = new ArrayList<Task>();

    public void createTask(int projId) {
        taskList.add(new Task(projId));
    }

    public void readAllTasks() {
        for (Task t : taskList) {
            System.out.println(t);
        }
    }

    public void readTaskByProjId(int id) {
        for (Task t : taskList) {
            if (id == t.getProjectId()) {
                System.out.println(t);
            }
        }
    }

    public void readTaskById(int id) {
        for (Task t : taskList) {
            if (id == t.getTaskId()) {
                System.out.println(t);
            }
        }
    }

    public void updateTask(int id, String description) {
        for (Task t : taskList) {
            if (id == t.getTaskId()) {
                t.setDescription(description);
            }
        }
    }

    public void deleteTask(int id) {
        for (Task t : taskList) {
            if (id == t.getTaskId()) {
                taskList.remove(t);
            }
        }
    }
    public void deleteAllTasks(){
        taskList.clear();
    }
}


