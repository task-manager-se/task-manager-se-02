package ru.zolov.tm;

import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        ProjectRepository projects = new ProjectRepository();
        TaskRepository tasks = new TaskRepository();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to TaskManager!");
        System.out.println("Type help for display list of commands");
        System.out.print("Input: ");

        String input;

        do {
            input = scanner.nextLine();
            int inputInt;
            int projectId;
            switch (input) {
                case "help":
                    System.out.println("For create project enter cp");
                    System.out.println("For display list of project enter sp");
                    System.out.println("For edit project enter ep");
                    System.out.println("For delete project enter dp");
                    System.out.println("For create task enter ct");
                    System.out.println("For display tasks enter st");
                    System.out.println("For edit task enter et");
                    System.out.println("For remove some task enter rt");
                    System.out.print("Input:");
                    break;
                case "cp":
                    System.out.print("Enter project name:");
                    input = scanner.nextLine();
                    projects.create(input);
                    System.out.println("[OK]");
                    System.out.print("Input:");
                    break;
                case "sp":
                    projects.readAll();
                    System.out.print("Input:");
                    break;
                case "ep":
                    System.out.println("Enter id:");
                    inputInt = scanner.nextInt();
                    System.out.println("Enter new project name");
                    input = scanner.nextLine();
                    projects.update(inputInt, input);
                    System.out.print("Input:");
                    break;
                case "dp":
                    System.out.println("Enter id:");
                    inputInt = scanner.nextInt();
                    projects.delete(inputInt);
                    System.out.println("DONE!");
                    System.out.print("Input:");
                    break;
                case "ct":
                    projects.readAll();
                    System.out.print("Enter project id: ");
                    projectId = scanner.nextInt();
                    tasks.createTask(projectId);
                    tasks.readTaskByProjId(projectId);
                    System.out.print("Input:");
                    break;
                case "dt":
                    tasks.readAllTasks();
                    System.out.print("Enter task id: ");
                    inputInt = scanner.nextInt();
                    tasks.deleteTask(inputInt);
                    System.out.println("DONE!");
                    System.out.print("Input:");
                    break;
                case "et":
                    tasks.readAllTasks();
                    System.out.print("Enter task id: ");
                    inputInt = scanner.nextInt();
                    System.out.println("Enter new description: ");
                    input = scanner.nextLine();
                    tasks.updateTask(inputInt, input);
                    System.out.println("DONE!");
                    tasks.readTaskById(inputInt);
                    System.out.print("Input:");
                    break;
                case "st":
                    tasks.readAllTasks();
                    System.out.print("Input:");
            }
        } while (!input.equals("exit"));

    }
}

