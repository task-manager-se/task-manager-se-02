package ru.zolov.tm.entity;

public class Task {
    private int projectId;
    private int taskId;
    private String description = "empty";
    private static int indexId;

    public Task(int projectId) {
        this.projectId = projectId;
        this.taskId = ++indexId;
    }


    public int getTaskId() {
        return taskId;
    }

    public int getProjectId() {
        return projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task[" +
                "projectId=" + projectId +
                ", taskId=" + taskId + ", description: " + description +
                ']';
    }
}
