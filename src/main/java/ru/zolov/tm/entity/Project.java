package ru.zolov.tm.entity;

public class Project {
    private int projectId;
    private String projectName;
    private static int indexId;



    public Project(String projectName) {
        this.projectId = ++indexId;
        this.projectName = projectName;
    }


    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return "Project[" +
                "projectId=" + projectId +
                ", projectName='" + projectName + '\'' +
                ']';
    }
}
